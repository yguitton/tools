# usegalaxy-fr-tools
This repository installs Galaxy tools on [usegalaxy.fr](https://usegalaxy.fr/)

It implements the Ansible role [galaxyproject/galaxy-tools](https://github.com/galaxyproject/ansible-galaxy-tools), and almost follow the usegalaxy.eu process [usegalaxy-eu/usegalaxy-eu-tools](https://github.com/usegalaxy-eu/usegalaxy-eu-tools)

## How to contribute
Unfortunately, so far, we didn't manage to allow "external" contributions. It's an issue with our Continous Integration (CI) process.
So, you are very welcome to contribute but we will have to add you as Developer on this GitLab repository.

## Work on branch

Before contributing to the repository, start by creating a new branch from the master branch.

## Install a new tool
1. Spot the appropriate tool list in [files/](files/)
2. Add your tool in both the `.yml` and the `.yml.lock`
3. Add a revision "identifiant" in the `.yml.lock`


[files/peak_calling.yml](files/peak_calling.yml)
```
---
install_repository_dependencies: true
install_resolver_dependencies: true
install_tool_dependencies: false
tools:
- name: sicer
  owner: devteam
  tool_panel_section_label: Peak Calling
```

[files/peak_calling.yml.lock](files/peak_calling.yml.lock)
```
install_repository_dependencies: true
install_resolver_dependencies: true
install_tool_dependencies: false
tools:
- name: sicer
  owner: devteam
  revisions:
  - 74c9214cc8e6
  tool_panel_section_label: Peak Calling
```

## Update an existing tool
1. Add a revision "identifiant" in appropriate the `.yml.lock`

Or

1. You can use through a terminal, this script [update-tool.py](scripts/update-tool.py)

```bash
./scripts/update-tool.py --owner iuc --name foobar files/peak_calling.yml`
```

## Update a bunch of tools
1. You can use through a terminal, this script [update-tool.py](scripts/update-tool.py)

```bash
./scripts/update-tool.py files/peak_calling.yml`
```
## Tool panel
### Order within the tool panel
After each tool installation, the tool panel is automaticaly sorted to match the order in yaml files. (for now only for workflow4metabolomics, proteore and galaxyP)
You can be add to the "sorted panel list", so if you want a "chronology" in your tool, you need to sort them in the `.yml` file

### Missing categories
If a category is missing, you can add it using this [tool_conf.ym](https://gitlab.com/ifb-elixirfr/usegalaxy-fr/infrastructure/-/blob/preprod/templates/galaxy/tool_conf.xml)

### Subdomains
The different subdomains are managed using filters here : [global_host_filters.py.j2](https://gitlab.com/ifb-elixirfr/usegalaxy-fr/infrastructure/-/blob/preprod/templates/galaxy/global_host_filters.py.j2)

## sanitize_whitelist.txt
If a tool need to display html as output. It have to be in this [sanitize_whitelist.txt](https://gitlab.com/ifb-elixirfr/usegalaxy-fr/infrastructure/-/blob/preprod/templates/galaxy/sanitize_whitelist.txt)

## Propose your contribution

Once you have edited your changes in your branch, push it to the repository and create a Merge Request.
